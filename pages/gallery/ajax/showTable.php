<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="col-md-2 col-sm-3 col-xs-6" onclick="showForm('ADD','')">
  <div class="img-list img-list-block">
    <div class="glyphicon glyphicon-plus icon-block"></div>
    <h6>New Gallery</h6>
  </div>
</div>
<?php
  $sqls   = "SELECT * FROM t_gallery WHERE is_active = 'Y' ORDER BY gallery_id DESC";
  $querys     = DbQuery($sqls,null);
  $json       = json_decode($querys, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];
  if($dataCount > 0){
    foreach ($rows as $value) {

      $sql   = "SELECT * FROM t_gallert_image WHERE gallery_id = '{$value['gallery_id']}' ORDER BY tg_id DESC LIMIT 0,1";
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true)['data'][0];

?>
<div class="col-md-2 col-sm-3 col-xs-6" onclick="showForm('EDIT','<?=$value['gallery_id']?>')">
  <div class="img-list" style="background-image: url(upload/<?=$json['tg_image']?>);"></div>
  <p class="text-center img-p"><?=$value['gallery_name']?></p>
</div>
<?php } } ?>
