
$(function () {
  showTable();
  $('.select2').select2();
  $('.datepicker').datepicker({
      autoclose: true
  });
});

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function checknumber(inputs){
  var valid = /^\d{0,4}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function showForm(value="",code=""){
  $.post("ajax/form.php",{value:value,code:code})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
      //changeProvince();
  });
}


function showFormPackage(id){
  $.post("ajax/formPackage.php",{id:id})
    .done(function( data ) {
      $('#show-package').html(data);
  });
}

function showPackage(code,fname){
  $.post("ajax/showPackage.php",{personCode:code})
    .done(function( data ) {
      $('#myModalLabelPackage').html("Package Member " + fname);
      $('#myModalPackage').modal({backdrop:'static'});
      $('#show-package').html(data);
  });
}




function changeProvince(){
  var code = $('#EMP_ADDR_PROVINCE').val();
  var name = $('#EMP_ADDR_DISTRICT_NAME').val();
  var res = code.split("|");
  $.post("ajax/getDistrict.php",{code:res[0],name:name})
    .done(function( data ) {
      $('#optionDistrict').html(data);
      setTimeout(changeDistrict, 300);
  });
}

function changeDistrict(){
  var code = $('#EMP_ADDR_DISTRICT').val();
  var name = $('#EMP_ADDR_SUBDISTRICT_NAME').val();
  //alert(code+":"+name);
  var res = code.split("|");
  $.post("ajax/getSubDistrict.php",{code:res[0],name:name})
    .done(function( data ) {
      $('#optionSubDistrict').html(data);
      changeSubDistrict();
  });
}

function changeSubDistrict(){
  var value = $('#EMP_ADDR_SUBDISTRICT').val();
  var res = value.split("|");
  $('#EMP_ADDR_POSTAL').val(res[1]);
}



function del(id,name){
  $.smkConfirm({
    text:'Are You Sure Delete Member :'+ name +'?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{typeActive:'DEL',code:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#000',
            barColor: '#fff',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$('#formEdit').on('submit', function(event) {
  event.preventDefault();
  if ($('#formEdit').smkValidate()) {
    $.ajax({
        url: 'ajax/editPackage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#000',
        barColor: '#fff',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formEdit').smkClear();
        showPackageMember(data.code);
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    });
  }
});

function showPackageMember(personCode)
{
  $.post("ajax/showPackage.php",{personCode:personCode})
    .done(function( data ) {
      $('#show-package').html(data);
  });
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formAdd').smkValidate()) {
    if(action == 'ADD'){
        if( $.smkEqualPass('#pass1', '#pass2') ){
          // Code here
            $.ajax({
                url: 'ajax/AED.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#000',
                barColor: '#fff',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formAddUsers').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModal').modal('toggle');
              }, 1000);
            });
        }
    }else{
        $.ajax({
            url: 'ajax/AED.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#000',
            barColor: '#fff',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formAddUsers').smkClear();
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  }
});
