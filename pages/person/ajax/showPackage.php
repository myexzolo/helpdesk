<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$personCode     = @$_POST['personCode'];

?>
<style>
th {text-align: center;}
</style>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
<table class="table table-bordered table-striped" id="tablePackage" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No.</th>
      <th>Package Name</th>
      <th>คงเหลือ</th>
      <th >หน่วย</th>
      <th >วันที่เริ่ม</th>
      <th >วันหมดอายุ</th>
      <th >สถานะ</th>
      <th ></th>
    </tr>
  </thead>
  <tbody>
    <?php

      $sql = "SELECT ps.*, iv.invoice_date
              FROM trans_package_person ps
              LEFT JOIN tb_invoice iv ON ps.invoice_code = iv.invoice_code and iv.status = 'A'
              where ps.person_code = '$personCode' and ps.status in ('A','U','E')
              order by ps.status ASC, ps.date_expire ASC";

      $querys     = DbQuery($sql,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {

        $statusText = "";

        $id     = $rows[$i]['id'];
        $status = $rows[$i]['status'];

        $style   = "style=\"cursor: pointer;\" onclick=\"showFormPackage($id)\"";

        if($status == "A" ){
          $statusText = "Active";
        }else if($status == "E" ){
          $statusText = "Expired";
        }else if($status == "U" ){
          $statusText = "Use up";
          $style   = "style=\"cursor:not-allowed;color:#ccc\"";
        }

    ?>
    <tr class="text-center">
      <td align="center"><?= $i+1; ?></td>
      <td align="left"><?= $rows[$i]['package_name']; ?></td>
      <td align="right"><?= $rows[$i]['use_package']."/".$rows[$i]['num_use']; ?></td>
      <td align="left"><?= $rows[$i]['package_unit']; ?></td>
      <td align="center"><?= DateThai($rows[$i]['date_start']); ?></td>
      <td align="center"><?= DateThai($rows[$i]['date_expire']); ?></td>
      <td align="center"><?= $statusText; ?></td>
      <td align="center" $disabled>
        <i class="fa fa-edit"  <?= $style ?>></i>
      </td>
    </tr>
  <?php } ?>
  </tbody>
</table>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
</div>
<script>
  $(function () {
    $('#tablePackage').DataTable({
     'paging'      : false,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
