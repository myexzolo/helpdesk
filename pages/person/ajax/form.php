<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = $_POST['value'];
$code         = isset($_POST['code'])?$_POST['code']:"";
$page_icon    = 'fa fa-circle-o';
$role_access  = '';

$COMPANY_CODE     = $_SESSION['branchCode'];
$PERSON_CODE      = "";
$PERSON_CODE_INT  = "";
$PERSON_TITLE     = "";
$PERSON_NAME      = "";
$PERSON_LASTNAME  = "";
$PERSON_SEX       = "";
$PERSON_ER_TEL    = "";
$PERSON_BIRTH_DATE = "";
$PERSON_NOTE = "";
$PERSON_GROUP = "";
$PERSON_GROUP_AGE = "";
$PERSON_NICKNAME  = "";
$PERSON_CARD_ID   = "";
$PERSON_HOME1_ADDR1 = "";
$PERSON_HOME1_ADDR2 = "";
$PERSON_HOME1_SUBDISTRICT = "";
$PERSON_HOME1_DISTRICT    = "";
$PERSON_HOME1_PROVINCE    = "";
$EMP_CODE_SALE            = "";
$PERSON_HOME1_POSTAL      = "";
$PERSON_TEL_MOBILE        = "";
$PERSON_TEL_MOBILE2       = "";
$PERSON_EMAIL             = "";
$PERSON_BILL_NAME         = "";
$PERSON_BILL_TAXNO        = "";
$PERSON_BILL_ADDR1        = "";
$PERSON_BILL_SUBDISTRICT  = "";
$PERSON_BILL_DISTRICT     = "";
$PERSON_BILL_PROVINCE     = "";
$PERSON_BILL_POSTAL       = "";
$PERSON_FAX               = "";
$PERSON_TEL_OFFICE        = "";
$PERSON_IMAGE             = "";

$branch_id = "";
$user_id   = "";

$disabled = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql = "SELECT * FROM  person
  where COMPANY_CODE ='$COMPANY_CODE' and PERSON_CODE = '$code' order by PERSON_CODE";

  //echo $sql;

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];


  $PERSON_CODE              = $row[0]['PERSON_CODE'];
  $PERSON_TITLE             = $row[0]['PERSON_TITLE'];
  $PERSON_NAME              = $row[0]['PERSON_NAME'];
  $PERSON_LASTNAME          = $row[0]['PERSON_LASTNAME'];
  $PERSON_SEX               = $row[0]['PERSON_SEX'];
  $PERSON_ER_TEL            = $row[0]['PERSON_ER_TEL'];
  $PERSON_BIRTH_DATE        = DateThai($row[0]['PERSON_BIRTH_DATE']);
  $PERSON_NOTE              = $row[0]['PERSON_NOTE'];
  $PERSON_GROUP             = $row[0]['PERSON_GROUP'];
  $PERSON_GROUP_AGE         = $row[0]['PERSON_GROUP_AGE'];
  $PERSON_NICKNAME          = $row[0]['PERSON_NICKNAME'];
  $PERSON_CARD_ID           = $row[0]['PERSON_CARD_ID'];
  $PERSON_HOME1_ADDR1       = $row[0]['PERSON_HOME1_ADDR1'];
  $PERSON_HOME1_ADDR2       = $row[0]['PERSON_HOME1_ADDR2'];
  $PERSON_HOME1_SUBDISTRICT = $row[0]['PERSON_HOME1_SUBDISTRICT'];
  $PERSON_HOME1_DISTRICT    = $row[0]['PERSON_HOME1_DISTRICT'];
  $PERSON_HOME1_PROVINCE    = $row[0]['PERSON_HOME1_PROVINCE'];
  $EMP_CODE_SALE            = $row[0]['EMP_CODE_SALE'];
  $PERSON_HOME1_POSTAL      = $row[0]['PERSON_HOME1_POSTAL'];
  $PERSON_TEL_MOBILE        = $row[0]['PERSON_TEL_MOBILE'];
  $PERSON_TEL_MOBILE2       = $row[0]['PERSON_TEL_MOBILE2'];
  $PERSON_EMAIL             = $row[0]['PERSON_EMAIL'];
  $PERSON_BILL_NAME         = $row[0]['PERSON_BILL_NAME'];
  $PERSON_BILL_TAXNO        = $row[0]['PERSON_BILL_TAXNO'];
  $PERSON_BILL_ADDR1        = $row[0]['PERSON_BILL_ADDR1'];
  $PERSON_BILL_SUBDISTRICT  = $row[0]['PERSON_BILL_SUBDISTRICT'];
  $PERSON_BILL_DISTRICT     = $row[0]['PERSON_BILL_DISTRICT'];
  $PERSON_BILL_PROVINCE     = $row[0]['PERSON_BILL_PROVINCE'];
  $PERSON_BILL_POSTAL       = $row[0]['PERSON_BILL_POSTAL'];
  $PERSON_FAX               = $row[0]['PERSON_FAX'];
  $PERSON_TEL_OFFICE        = $row[0]['PERSON_TEL_OFFICE'];
  $PERSON_IMAGE             = $row[0]['PERSON_IMAGE'];
}

$optionDistrict = "";
if($action == 'ADD'){
    $btn = 'Save changes';
}

$optionTitle          = getoptionDataMaster('PRENAME',$PERSON_TITLE);
$optionProvince       = getoptionProvince($PERSON_HOME1_PROVINCE);
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="user_id" value="<?=$user_id?>">
<input type="hidden" id="EMP_ADDR_DISTRICT_NAME" value="<?=$EMP_ADDR_DISTRICT?>">
<input type="hidden" id="EMP_ADDR_SUBDISTRICT_NAME" value="<?=$EMP_ADDR_SUBDISTRICT?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสสมาชิก</label>
        <input value="<?= $PERSON_CODE ?>" name="PERSON_CODE" type="text" maxlength="15" class="form-control text-uppercase" placeholder="Code" required readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>คำนำหน้า</label>
        <select name="PERSON_TITLE" data-smk-msg="&nbsp;" id="PERSON_TITLE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
          <option value="" selected></option>
          <?= $optionTitle; ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ</label>
          <input value="<?= $PERSON_NAME ?>" name="PERSON_NAME" type="text" class="form-control" placeholder="Name" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สกุล</label>
          <input value="<?= $PERSON_LASTNAME ?>" name="PERSON_LASTNAME" type="text" class="form-control" placeholder="LastName" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อเล่น</label>
          <input value="<?= $PERSON_NICKNAME ?>" name="PERSON_NICKNAME" type="text" class="form-control text-uppercase" placeholder="NickName" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เพศ</label>
        <div class="form-control" style="border-style: hidden;">
          <input type="radio" data-smk-msg="&nbsp;" name="PERSON_SEX" id="gender_m" class="minimal" value="ชาย" <?= $PERSON_SEX == 'ชาย'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
          <input type="radio" data-smk-msg="&nbsp;" name="PERSON_SEX" id="gender_f" class="minimal" value="หญิง" <?= $PERSON_SEX == 'หญิง'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่บัตรประชาชน</label>
        <input value="<?= $PERSON_CARD_ID ?>" name="PERSON_CARD_ID" type="text"  onkeyup="checknumber(this)" class="form-control" placeholder="ID Card" data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันเดือนปีเกิด</label>
        <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= $PERSON_BIRTH_DATE ?>" required name="PERSON_BIRTH_DATE" type="text" data-provide="datepicker" data-date-language="th-th" readonly style="background-color:#fff;">
      </div>
  </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ที่อยู่</label>
        <input value="<?= $PERSON_HOME1_ADDR1?>" name="PERSON_HOME1_ADDR1" type="text" class="form-control" placeholder="Address .." >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ที่อยู่2</label>
        <input value="<?= $PERSON_HOME1_ADDR2 ?>" name="PERSON_HOME1_ADDR2" type="text" class="form-control" placeholder="Address .." >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>จังหวัด</label>
        <select name="EMP_ADDR_PROVINCE" id="EMP_ADDR_PROVINCE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required onchange="changeProvince();">
          <?= $optionProvince; ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>อำเภอ/เขต</label>
        <div id="optionDistrict" >
        <select name="EMP_ADDR_DISTRICT" id="EMP_ADDR_DISTRICT" class="form-control select2" style="width: 100%;"  onchange="changeDistrict();">
          <option value="" selected></option>
        </select>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ตำบล/แขวง</label>
        <div id="optionSubDistrict" >
        <select name="EMP_ADDR_SUBDISTRICT" id="EMP_ADDR_SUBDISTRICT" class="form-control select2" style="width: 100%;" onchange="changeProvince();" required>
          <option value="" selected></option>
        </select>
        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>รหัสไปรษณีย์</label>
        <input value="<?= $EMP_ADDR_POSTAL ?>" name="EMP_ADDR_POSTAL" id="EMP_ADDR_POSTAL" type="text" class="form-control" placeholder="Zip Code" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันที่รับทำงาน</label>
        <input value="<?= $EMP_DATE_INCOME ?>" name="EMP_DATE_INCOME" type="date" class="form-control" placeholder="วันที่รับทำงาน" >
      </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>User Login</label>
      <input value="<?=$EMP_USER?>" name="EMP_USER" autocomplete="off" type="text" class="form-control" placeholder="User Login" required>
    </div>
  </div>
  <?php if($action == 'ADD'){ ?>
  <div class="col-md-5">
    <div class="form-group">
      <label>Password</label>
      <input value="" name="EMP_PSW" id="pass1" type="password" autocomplete="new-password" class="form-control" placeholder="Password" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Confirm Password</label>
      <input value="" id="pass2" type="password" class="form-control" placeholder="Confirm Password" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Gen Pass</label>
      <button type="button" onclick="CreatePass()" class="btn btn-primary btn-flat btn-block">Gen Pass</button>
    </div>
  </div>
  <?php } ?>
  <div class="col-md-4">
    <div class="form-group">
      <label>ประเภทพนักงาน</label>
      <select name="EMP_TYPE" id="EMP_TYPE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="" selected></option>
        <?= $optionEmpType; ?>
      </select>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>อีเมล์</label>
      <input value="<?=$EMP_EMAIL?>" name="EMP_EMAIL" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>เบอร์โทร</label>
      <input value="<?=$EMP_TEL?>" name="EMP_TEL" type="tel" class="form-control" placeholder="Tel" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ค่าจ้าง Class/ครั้ง</label>
      <input value="<?=$EMP_WAGE?>" name="EMP_WAGE" type="number" class="form-control" placeholder=""  style="text-align:right;" >
    </div>
  </div>
  <div class="col-md-9">
    <div class="form-group">
      <label>สิทธิพนักงาน</label>
      <div class="checkbox">
        <label><input type="checkbox" name="EMP_IS_TRAINER" <?= $EMP_IS_TRAINER == 'Y'? 'checked':''?> value="Y">Trainer</label>&nbsp;&nbsp;
        <label><input type="checkbox" name="EMP_IS_INSTRUCTOR"  <?= $EMP_IS_INSTRUCTOR == 'Y'? 'checked':''?> value="Y">Instructor</label>&nbsp;&nbsp;
        <label><input type="checkbox" name="EMP_IS_SALE"  <?= $EMP_IS_SALE == 'Y'? 'checked':''?> value="Y">Sale</label>&nbsp;&nbsp;
        <label><input type="checkbox" name="EMP_IS_STAFF"  <?= $EMP_IS_STAFF == 'Y'? 'checked':''?> value="Y">Manager</label>&nbsp;&nbsp;
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Image</label>
      <input name="user_img" type="file" class="form-control" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>
        <img src="<?= $user_img ?>" onerror="this.onerror='';this.src='images/user.png'" style="height: 60px;">
      </label>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>
