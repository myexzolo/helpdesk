// JS Login //
showForm();

function showForm(){
  $.post("ajax/form.php",{})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}


$('#form').on('submit', function(event) {
  event.preventDefault();
  $('.icons').remove('span');
  if ($('#form').smkValidate()) {
    if( $.smkEqualPass('#pass1', '#pass2') ){
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
        $('#form').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        if(data.status == 'success'){
          window.location = '../../pages/login/';
        }
      // }, 1000);
    });
  }
}
});
