<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = isset($_POST['id'])?$_POST['id']:"";
$page_icon    = 'fa fa-circle-o';
$role_access  = '';
$arr_page_list = array();

$branch_code      = "";
$cname            = "";
$branch_name      = "";
$branch_address   = "";
$branch_tel       = "";
$branch_fax       = "";
$branch_tax       = "";
$office_hours     = "";
$branch_open      = "";
$branch_close     = "";
$office_hours2    = "";
$branch_open2     = "";
$branch_close2    = "";
$is_active        = "";
$branch_logo      = "";
$lat              = "";
$lng              = "";
$image_checkin    = "";


if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql   = "SELECT * FROM t_branch WHERE branch_id = '$id' ORDER BY branch_id DESC";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $row        = $json['data'];

  $branch_code      = $row[0]['branch_code'];
  $cname            = $row[0]['cname'];
  $branch_name      = $row[0]['branch_name'];
  $branch_address   = $row[0]['branch_address'];
  $branch_tel       = $row[0]['branch_tel'];
  $branch_fax       = $row[0]['branch_fax'];
  $branch_tax       = $row[0]['branch_tax'];
  $office_hours     = $row[0]['office_hours'];
  $branch_open      = $row[0]['branch_open'];
  $branch_close     = $row[0]['branch_close'];
  $office_hours2    = $row[0]['office_hours2'];
  $branch_open2     = $row[0]['branch_open2'];
  $branch_close2    = $row[0]['branch_close2'];
  $is_active        = $row[0]['is_active'];
  $lat              = $row[0]['lat'];
  $lng              = $row[0]['lng'];

  $branch_logo      = isset($row[0]['branch_logo'])?$row[0]['branch_logo']:"";
  $image_checkin    = isset($row[0]['image_checkin'])?$row[0]['image_checkin']:"";

}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="branch_id" value="<?=$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>Branch Code</label>
        <input value="<?=$branch_code?>" name="branch_code" type="text" maxlength="8" class="form-control text-uppercase" placeholder="Code" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Name</label>
        <input value="<?=$cname?>" name="cname" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Branch Name</label>
        <input value="<?= $branch_name?>" name="branch_name" type="text" class="form-control" placeholder="Name" required >
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>Branch Address</label>
        <input value="<?=$branch_address?>" name="branch_address" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Tax No.</label>
        <input value="<?= $branch_tax?>" name="branch_tax" type="text" class="form-control" placeholder="TAX" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Tel</label>
        <input value="<?= $branch_tel?>" name="branch_tel" type="text" class="form-control" placeholder="Tel Number" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Fax</label>
        <input value="<?= $branch_fax?>" name="branch_fax" type="text" class="form-control" placeholder="Fax Number" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>วันที่ทำการ</label>
        <input value="<?= $office_hours?>" name="office_hours" type="text" class="form-control" placeholder="จ. - ศ." >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เวลาเปิด</label>
        <input value="<?= $branch_open?>" name="branch_open" type="text" class="form-control" placeholder="00:00" >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เวลาปิด</label>
        <input value="<?= $branch_close?>" name="branch_close" type="text" class="form-control" placeholder="00:00" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>วันที่ทำการ 2</label>
        <input value="<?= $office_hours2?>" name="office_hours2" type="text" class="form-control" placeholder="ส. - อา." >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เวลาเปิด</label>
        <input value="<?= $branch_open2?>" name="branch_open2" type="text" class="form-control" placeholder="00:00" >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เวลาปิด</label>
        <input value="<?= $branch_close2?>" name="branch_close2" type="text" class="form-control" placeholder="00:00" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>latitude</label>
        <input value="<?= $lat ?>" name="lat" type="text" class="form-control" placeholder="" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>longitude</label>
        <input value="<?= $lng ?>" name="lng" type="text" class="form-control" placeholder="" >
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>Logo (400x400 px)</label>
        <input name="branch_logo" type="file" class="form-control" accept="image/x-png,image/gif,image/jpeg" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <img src="<?= $branch_logo ?>" onerror="this.onerror='';this.src='../../image/nologo.png'" style="height: 60px;">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>Map Point (128x128 px)</label>
        <input name="image_checkin" type="file" class="form-control" accept="image/x-png,image/gif,image/jpeg" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <img src="<?= $image_checkin ?>" onerror="this.onerror='';this.src='../../image/picture.png'" style="height: 60px;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=$is_active=='Y'?"selected":""?>>ACTIVE</option>
          <option value="N" <?=$is_active=='N'?"selected":""?>>NO ACTIVE</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>
