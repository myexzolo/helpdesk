<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>Code</td>
      <td>Name</td>
      <td>ที่อยู่</td>
      <td>เบอร์โทร</td>
      <td>เวลาเปิด</td>
      <td>เวลาปิด</td>
      <td>สถานะ</td>
      <td style="width:70px;">Edit</td>
      <td style="width:70px;">Del</td>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_branch ORDER BY branch_id DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
    ?>
    <tr class="text-center">
      <td><?=$i + 1;?></td>
      <td><?=$rows[$i]['branch_code'];?></td>
      <td><?=$rows[$i]['branch_name'];?></td>
      <td><?=$rows[$i]['branch_address'];?></td>
      <td><?=$rows[$i]['branch_tel'];?></td>
      <td><?=$rows[$i]['branch_open'];?></td>
      <td><?=$rows[$i]['branch_close'];?></td>
      <td><?=$rows[$i]['is_active']=='Y'?"ACTIVE":"NO ACTIVE";?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['branch_id']?>')">
          <i class="fa fa-edit"></i>
        </button>
      </td>
      <td>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['branch_id']?>','<?=$rows[$i]['branch_name']?>')">
          <i class="fa fa-trash-o"></i>
        </button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
