<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

$week = isset($_POST['week'])?$_POST['week']:"0";

if($_SESSION['branchCode'] != ""){
  $con = " and s.branch_code = '".$_SESSION['branchCode']."' ";
}


$d = strtotime("$week week 0 day");

$start_week = strtotime("monday",$d);
$end_week = strtotime("next sunday",$d);
$start = date("Y-m-d",$start_week);
$end = date("Y-m-d",$end_week);

//echo $week.">>>".$start.">>>".$end;



$sql   = "SELECT sc.*,c.name_class,c.image_class,e.EMP_NICKNAME
          FROM tb_schedule_class_day sc, t_classes c, data_mas_employee e, tb_schedule_class s
          where  sc.date_class between '$start' and '$end' and sc.id_class = c.id_class and sc.EMP_CODE = e.EMP_CODE and s.is_active = 'Y'
          and s.schedule_id = sc.schedule_id $con";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

for($i =0; $i < $dataCount; $i++ )
{
  $id_class   = $rows[$i]['id_class'];
  $EMP_CODE   = $rows[$i]['EMP_CODE'];
  $day        = $rows[$i]['day'];
  $row        = $rows[$i]['row'];
  $unit       = $rows[$i]['unit'];
  $personJoin   = $rows[$i]['person_join'];
  $name_class   = $rows[$i]['name_class'];
  $time_start   = $rows[$i]['time_start'];
  $time_end     = $rows[$i]['time_end'];
  $EMP_NICKNAME = $rows[$i]['EMP_NICKNAME'];
  $EMP_WAGE     = $rows[$i]['EMP_WAGE'];
  $image_class  = $rows[$i]['image_class'];


  $classArr[$day][$row]['id']           = $day."_".$row;
  $classArr[$day][$row]['id_class']     = $id_class;
  $classArr[$day][$row]['EMP_CODE']     = $EMP_CODE;
  $classArr[$day][$row]['unit']         = $unit;
  $classArr[$day][$row]['person_join']  = $personJoin;
  $classArr[$day][$row]['time_start']   = $time_start;
  $classArr[$day][$row]['time_end']     = $time_end;
  $classArr[$day][$row]['name_class']   = $name_class;
  $classArr[$day][$row]['empNickName']  = $EMP_NICKNAME;
  $classArr[$day][$row]['EMP_WAGE']     = $EMP_WAGE;
  $classArr[$day][$row]['image_class']     = $image_class;
}

?>

<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
  margin-bottom: 5px;
}
th {
  text-align: center;
}
td {
  background-color: #fff5f5;
}

}
.info-box {
  margin-bottom: 0px;
}

.boxClass {
  height: 170px;
}
</style>
<table class="table table-bordered table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +0 day")); ?></th>
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +1 day")); ?></th>
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +2 day")); ?></th>
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +3 day")); ?></th>
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +4 day")); ?></td>
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +5 day")); ?></th>
      <th style="width:14%"><?= date("D d/m",strtotime($start ." +6 day")); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
      for($i=1 ; $i<7; $i++)
      {
     ?>
     <tr>
       <td>
          <div class="info-box boxClass" style="margin-bottom: 0px;">
            <div class="box-body">
              <?php if(isset($classArr[1][$i]['name_class'])){ ?>
              <div class="" align="center"><img src="<?= $classArr[1][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
              <div style="border: 1px solid #ccc;height:1px;"></div>
              <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[1][$i]['time_start']." - ".$classArr[1][$i]['time_end'] ?></b></div>
              <div class="pull-left cut-text" style="width:60%"><?= $classArr[1][$i]['empNickName'] ?></div>
              <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
                <?= @$classArr[1][$i]['person_join']."/".@$classArr[1][$i]['unit'] ?> <i class="fa fa-group"></i>
              </div>
            <?php } ?>
          </div>
        </div>
       </td>
       <td>
         <div class="info-box boxClass" style="margin-bottom: 0px;">
           <div class="box-body">
         <?php if(isset($classArr[2][$i]['name_class'])){ ?>
           <div class="" align="center"><img src="<?= $classArr[2][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
           <div style="border: 1px solid #ccc;height:1px;"></div>
         <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[2][$i]['time_start']." - ".$classArr[2][$i]['time_end'] ?></b></div>
         <div class="pull-left cut-text" style="width:60%"><?= $classArr[2][$i]['empNickName'] ?></div>
         <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
           <?= @$classArr[2][$i]['person_join']."/".@$classArr[2][$i]['unit'] ?> <i class="fa fa-group"></i>
         </div>
       <?php } ?>
     </div>
   </div>
       </td>
       <td>
         <div class="info-box boxClass" style="margin-bottom: 0px;">
           <div class="box-body">
         <?php if(isset($classArr[3][$i]['name_class'])){ ?>
           <div class="" align="center"><img src="<?= $classArr[3][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
           <div style="border: 1px solid #ccc;height:1px;"></div>
         <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[3][$i]['time_start']." - ".$classArr[3][$i]['time_end'] ?></b></div>
         <div class="pull-left cut-text" style="width:60%"><?= $classArr[3][$i]['empNickName'] ?></div>
         <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
           <?= @$classArr[3][$i]['person_join']."/".@$classArr[3][$i]['unit'] ?> <i class="fa fa-group"></i>
         </div>
       <?php } ?>
     </div>
   </div>
       </td>
       <td>
         <div class="info-box boxClass" style="margin-bottom: 0px;">
           <div class="box-body">
         <?php if(isset($classArr[4][$i]['name_class'])){ ?>
           <div class="" align="center"><img src="<?= $classArr[4][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
           <div style="border: 1px solid #ccc;height:1px;"></div>
         <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[4][$i]['time_start']." - ".$classArr[4][$i]['time_end'] ?></b></div>
         <div class="pull-left cut-text" style="width:60%"><?= $classArr[4][$i]['empNickName'] ?></div>
         <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
           <?= @$classArr[1][$i]['person_join']."/".@$classArr[4][$i]['unit'] ?> <i class="fa fa-group"></i>
         </div>
       <?php } ?>
     </div>
   </div>
       </td>
       <td>
         <div class="info-box boxClass" style="margin-bottom: 0px;">
           <div class="box-body">
         <?php if(isset($classArr[5][$i]['name_class'])){ ?>
           <div class="" align="center"><img src="<?= $classArr[5][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
           <div style="border: 1px solid #ccc;height:1px;"></div>
         <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[5][$i]['time_start']." - ".$classArr[5][$i]['time_end'] ?></b></div>
         <div class="pull-left cut-text" style="width:60%"><?= $classArr[5][$i]['empNickName'] ?></div>
         <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
           <?= @$classArr[5][$i]['person_join']."/".@$classArr[5][$i]['unit'] ?> <i class="fa fa-group"></i>
         </div>
       <?php } ?>
     </div>
   </div>
       </td>
       <td>
         <div class="info-box boxClass" style="margin-bottom: 0px;">
           <div class="box-body">
         <?php if(isset($classArr[6][$i]['name_class'])){ ?>
           <div class="" align="center"><img src="<?= $classArr[6][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
           <div style="border: 1px solid #ccc;height:1px;"></div>
         <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[6][$i]['time_start']." - ".$classArr[6][$i]['time_end'] ?></b></div>
         <div class="pull-left cut-text" style="width:60%"><?= $classArr[6][$i]['empNickName'] ?></div>
         <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
           <?= @$classArr[6][$i]['person_join']."/".@$classArr[6][$i]['unit'] ?> <i class="fa fa-group"></i>
         </div>
       <?php } ?>
     </div>
   </div>
       </td>
       <td>
         <div class="info-box boxClass" style="margin-bottom: 0px;">
           <div class="box-body">
             <?php if(isset($classArr[7][$i]['name_class'])){ ?>
               <div class="" align="center"><img src="<?= $classArr[7][$i]['image_class'] ;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></div>
               <div style="border: 1px solid #ccc;height:1px;"></div>
             <div style="font-size:14px;margin-bottom:5px;margin-top:2px;"class="pull-left cut-text"><b><?= $classArr[7][$i]['time_start']." - ".$classArr[7][$i]['time_end'] ?></b></div>
             <div class="pull-left cut-text" style="width:60%"><?= $classArr[7][$i]['empNickName'] ?></div>
             <div class="pull-right users-list-date" align="right" style="margin-top:2px;width:40%;">
               <?= @$classArr[7][$i]['person_join']."/".@$classArr[7][$i]['unit'] ?> <i class="fa fa-group"></i>
             </div>
           <?php } ?>
         </div>
       </div>
      </td>
   </tr>
   <?php } ?>
 </div>
