<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="google-site-verification" content="HL-4q2f8iiaLDlSvEOkSSsQuQxuPpDzXxHcOvdGBc3c" />
      <title>ระบบบริหารจัดการคิว | Dashboard</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Dashboard
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
            <section class="col-lg-5 connectedSortable ui-sortable">
          <!-- Custom tabs (Charts with tabs)-->
              <div class="box box-success" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1)";>
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="fa fa-venus-mars"></i>
                  <h3 class="box-title">สรุปรายงานกลุ่มลูกค้าแยกตามเพศ</h3>
                </div>
                <div class="box-body">
                    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                </div>
              </div>

             <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
               <div class="box-header ui-sortable-handle" style="cursor: move;">
                 <i class="ion fa-map"></i>
                 <h3 class="box-title">สรุปรายงานกลุ่มลูกค้าแยกตามพื่นที่</h3>
                 <div class="pull-right">
                   <select id="provinces_id" class="form-control" onchange="getMap()" style="width:200px;">
                     <option value="">ทุกพื่นที่</option>
                     <?php
                       $sqlz   = "SELECT * FROM t_provinces";
                       $queryz = DbQuery($sqlz,null);
                       $jsonz  = json_decode($queryz, true);
                       $rowz   = $jsonz['data'];
                         foreach ($rowz as $valuez) { ?>
                           <option value="<?=$valuez['id']?>"><?=$valuez['name_th']?></option>
                     <?php } ?>
                   </select>
                 </div>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                 <div id="chartMap" style="width: 100%;"></div>
               </div>
             </div>

            <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">To Do List</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body"></div>
              <!-- /.box-body -->
              <div class="box-footer clearfix no-border">
                <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
              </div>
            </div>

            <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="fa fa-envelope"></i>

                <h3 class="box-title">Quick Email</h3>
              </div>
              <div class="box-body">

              </div>
              <div class="box-footer clearfix">
                <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                  <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </div>
        </section>
        <section class="col-lg-7 connectedSortable ui-sortable">
          <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1)";>
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-map"></i>
              <h3 class="box-title">รายงานผู้มารับบริการ</h3>
            </div>
            <div class="box-body">
              <div id="map" style="width: 100%; height:650px;"></div>
              <br>
              <div id="chartTable" style="width: 100%;"></div>
            </div>
          </div>
        </section>
          </div>
              <!-- /.row -->
        </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script src='js/jquery-jvectormap-th-mill.js'></script>
      <script>
      showClass(0);
      function getclassWeek(week)
      {
        var num =  parseInt($("#weekofDay").val());
        if(week != 0){
          num = (num + week);
        }
        $("#weekofDay").val(num);
        showClass(num);
      }
      </script>
    </body>
  </html>
