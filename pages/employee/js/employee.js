
showTable();

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",id=""){
  $.post("ajax/form.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
      changeProvince();
  });
}


function changeProvince(){
  var code = $('#EMP_ADDR_PROVINCE').val();
  var name = $('#EMP_ADDR_DISTRICT_NAME').val();
  var res = code.split("|");
  $.post("ajax/getDistrict.php",{code:res[0],name:name})
    .done(function( data ) {
      $('#optionDistrict').html(data);
      setTimeout(changeDistrict, 300);
  });
}

function changeDistrict(){
  var code = $('#EMP_ADDR_DISTRICT').val();
  var name = $('#EMP_ADDR_SUBDISTRICT_NAME').val();
  //alert(code+":"+name);
  var res = code.split("|");
  $.post("ajax/getSubDistrict.php",{code:res[0],name:name})
    .done(function( data ) {
      $('#optionSubDistrict').html(data);
      changeSubDistrict();
  });
}

function changeSubDistrict(){
  var value = $('#EMP_ADDR_SUBDISTRICT').val();
  var res = value.split("|");
  $('#EMP_ADDR_POSTAL').val(res[1]);
}



function del(code,name){
  $.smkConfirm({
    text:'Are You Sure Delete Employee : '+ name +'?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',EMP_CODE:code})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#000',
            barColor: '#fff',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}



$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formAdd').smkValidate()) {
    if(action == 'ADD'){
        if( $.smkEqualPass('#pass1', '#pass2') ){
          // Code here
            $.ajax({
                url: 'ajax/AED.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#000',
                barColor: '#fff',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formAddUsers').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModal').modal('toggle');
              }, 1000);
            });
        }
    }else{
        $.ajax({
            url: 'ajax/AED.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#000',
            barColor: '#fff',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formAddUsers').smkClear();
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  }
});
