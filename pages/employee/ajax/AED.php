<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action       = isset($_POST['action'])?$_POST['action']:"";
$companycode  = $_SESSION['branchCode'];

$EMP_CODE         = isset($_POST['EMP_CODE'])?$_POST['EMP_CODE']:"";
$EMP_TITLE        = isset($_POST['EMP_TITLE'])?$_POST['EMP_TITLE']:"";
$EMP_NAME         = isset($_POST['EMP_NAME'])?$_POST['EMP_NAME']:"";
$EMP_LASTNAME     = isset($_POST['EMP_LASTNAME'])?$_POST['EMP_LASTNAME']:"";
$EMP_POSITION     = isset($_POST['EMP_POSITION'])?$_POST['EMP_POSITION']:"";
$EMP_DEPARTMENT   = isset($_POST['EMP_DEPARTMENT'])?$_POST['EMP_DEPARTMENT']:"";
$EMP_SEX          = isset($_POST['EMP_SEX'])?$_POST['EMP_SEX']:"";
$EMP_BIRTH_DATE   = isset($_POST['EMP_BIRTH_DATE'])?$_POST['EMP_BIRTH_DATE']:"";
$EMP_TYPE         = isset($_POST['EMP_TYPE'])?$_POST['EMP_TYPE']:"";
$EMP_GROUP        = isset($_POST['EMP_GROUP'])?$_POST['EMP_GROUP']:"";
$EMP_NICKNAME     = isset($_POST['EMP_NICKNAME'])?$_POST['EMP_NICKNAME']:"";
$EMP_DATE_INCOME  = isset($_POST['EMP_DATE_INCOME'])?$_POST['EMP_DATE_INCOME']:"";
$EMP_USER         = isset($_POST['EMP_USER'])?$_POST['EMP_USER']:"";
$EMP_PSW          = isset($_POST['EMP_PSW'])?$_POST['EMP_PSW']:$EMP_CODE;
$EMP_CARD_ID      = isset($_POST['EMP_CARD_ID'])?$_POST['EMP_CARD_ID']:"";
$EMP_ADDRESS1     = isset($_POST['EMP_ADDRESS1'])?$_POST['EMP_ADDRESS1']:"";
$EMP_ADDRESS2     = isset($_POST['EMP_ADDRESS2'])?$_POST['EMP_ADDRESS2']:"";
$EMP_ADDR_SUBDISTRICT   = isset($_POST['EMP_ADDR_SUBDISTRICT'])?$_POST['EMP_ADDR_SUBDISTRICT']:"";
$EMP_ADDR_DISTRICT      = isset($_POST['EMP_ADDR_DISTRICT'])?$_POST['EMP_ADDR_DISTRICT']:"";
$EMP_ADDR_PROVINCE      = isset($_POST['EMP_ADDR_PROVINCE'])?$_POST['EMP_ADDR_PROVINCE']:"";
$EMP_ADDR_POSTAL        = isset($_POST['EMP_ADDR_POSTAL'])?$_POST['EMP_ADDR_POSTAL']:"";
$EMP_EMAIL        = isset($_POST['EMP_EMAIL'])?$_POST['EMP_EMAIL']:"";
$EMP_TEL          = isset($_POST['EMP_TEL'])?$_POST['EMP_TEL']:"";
$EMP_DATE_RETRY   = isset($_POST['EMP_DATE_RETRY'])?$_POST['EMP_DATE_RETRY']:"";
$EMP_PICTURE      = isset($_POST['EMP_PICTURE'])?$_POST['EMP_PICTURE']:"";
$EMP_IS_TRAINER   = isset($_POST['EMP_IS_TRAINER'])?$_POST['EMP_IS_TRAINER']:"";
$EMP_IS_STAFF     = isset($_POST['EMP_IS_STAFF'])?$_POST['EMP_IS_STAFF']:"";
$EMP_IS_INSTRUCTOR = isset($_POST['EMP_IS_INSTRUCTOR'])?$_POST['EMP_IS_INSTRUCTOR']:"";
$EMP_IS_SALE        = isset($_POST['EMP_IS_SALE'])?$_POST['EMP_IS_SALE']:"";
$EMP_WAGE           = isset($_POST['EMP_WAGE'])?$_POST['EMP_WAGE']:"";

$user_id_update = $_SESSION['member'][0]['user_id'];

if($action != "DEL")
{
  $EMP_ADDR_SUBDISTRICT = explode("|",$EMP_ADDR_SUBDISTRICT);
  $EMP_ADDR_SUBDISTRICT = $EMP_ADDR_SUBDISTRICT[0];

  $EMP_ADDR_DISTRICT = explode("|",$EMP_ADDR_DISTRICT);
  $EMP_ADDR_DISTRICT = $EMP_ADDR_DISTRICT[1];

  $EMP_ADDR_PROVINCE = explode("|",$EMP_ADDR_PROVINCE);
  $EMP_ADDR_PROVINCE = $EMP_ADDR_PROVINCE[1];

  $user_id        = isset($_POST['user_id'])?$_POST['user_id']:"";
  $user_login     = $EMP_USER;
  $user_name      = $EMP_NAME;
  $user_last      = $EMP_LASTNAME;
  $user_email     = $EMP_EMAIL;
  $user_password  = @md5($EMP_PSW);
  $is_active      = "Y";
  $role_list      = "3";
  $branch_code    = $companycode;
  $department_code  = $EMP_DEPARTMENT;
  $note1          = "";
  $note2          = "";

  $user_img   = "";
  $updateImg  = "";

  if($EMP_IS_STAFF == "Y")
  {
    $role_list      = "2,3";
  }else{
    $role_list      = "3";
  }

  $path = "images/";
  if(isset($_FILES["user_img"])){
    $user_img = resizeImageToBase64($_FILES["user_img"],'','148','100',$user_id_update,$path);

    if(!empty($user_img)){
        $updateImg    =  "user_img = '$user_img',";
    }
  }

// --ADD EDIT DELETE Module-- //
  if(empty($user_id)){
    $sql   = "INSERT INTO t_user (user_login,user_password,user_email,user_name,user_last,
             is_active,role_list,user_img,user_since,user_id_update,note1,note2,branch_code,department_code)
             VALUES('$user_login','$user_password','$user_email','$user_name','$user_last',
             '$is_active','$role_list','$user_img',NOW(),'$user_id_update','$note1','$note2','$branch_code','$department_code')";
  }else{
    $sql = "UPDATE t_user SET
              user_login     = '$user_login',
              user_name      = '$user_name',
              user_last      = '$user_last',
              user_email     = '$user_email',
              note1          = '$note1',
              note2          = '$note2',
              branch_code    = '$branch_code',
              role_list      = '$role_list',
              department_code  = '$department_code',
              ".$updateImg."
              is_active      = '$is_active',
              update_date    = NOW(),
              user_id_update = '$user_id_update'
              WHERE user_id = '$user_id'";

      if($user_id_update == $user_id){
        $sqls   = "SELECT * FROM t_user WHERE user_id = '$user_id'";
        $query      = DbQuery($sqls,null);
        $json       = json_decode($query, true);
        $rows       = $json['data'];

        $_SESSION['member'] = $rows;
      }
  }

  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];
}
if($action == "ADD"){
  $sql   = "INSERT INTO data_mas_employee (
            COMPANY_CODE,
            EMP_CODE,EMP_TITLE,
            EMP_NAME,EMP_LASTNAME,
            EMP_POSITION,EMP_DEPARTMENT,
            EMP_SEX,EMP_BIRTH_DATE,
            EMP_TYPE,EMP_GROUP,
            EMP_NICKNAME,EMP_DATE_INCOME,
            EMP_USER,EMP_PSW,
            EMP_CARD_ID,EMP_ADDRESS1,
            EMP_ADDRESS2,EMP_ADDR_SUBDISTRICT,
            EMP_ADDR_DISTRICT,EMP_ADDR_PROVINCE,
            EMP_ADDR_POSTAL,EMP_EMAIL,EMP_WAGE,
            EMP_TEL,EMP_IS_TRAINER,EMP_IS_STAFF,EMP_IS_INSTRUCTOR,EMP_IS_SALE)
           VALUES(
             '$companycode',
             '$EMP_CODE','$EMP_TITLE',
             '$EMP_NAME','$EMP_LASTNAME',
             '$EMP_POSITION','$EMP_DEPARTMENT',
             '$EMP_SEX','$EMP_BIRTH_DATE',
             '$EMP_TYPE','$EMP_GROUP',
             '$EMP_NICKNAME','$EMP_DATE_INCOME',
             '$EMP_USER','$EMP_PSW',
             '$EMP_CARD_ID','$EMP_ADDRESS1',
             '$EMP_ADDRESS2','$EMP_ADDR_SUBDISTRICT',
             '$EMP_ADDR_DISTRICT','$EMP_ADDR_PROVINCE',
             '$EMP_ADDR_POSTAL','$EMP_EMAIL','$EMP_WAGE',
             '$EMP_TEL','$EMP_IS_TRAINER','$EMP_IS_STAFF','$EMP_IS_INSTRUCTOR','$EMP_IS_SALE'
           )";
}else if($action == "EDIT"){
  $sql = "UPDATE data_mas_employee SET
          EMP_TITLE           ='$EMP_TITLE',
          EMP_NAME            ='$EMP_NAME',
          EMP_LASTNAME        ='$EMP_LASTNAME',
          EMP_POSITION        ='$EMP_POSITION',
          EMP_DEPARTMENT      ='$EMP_DEPARTMENT',
          EMP_SEX             ='$EMP_SEX',
          EMP_BIRTH_DATE      ='$EMP_BIRTH_DATE',
          EMP_USER            ='$EMP_USER',
          EMP_TYPE            ='$EMP_TYPE',
          EMP_NICKNAME        ='$EMP_NICKNAME',
          EMP_DATE_INCOME     ='$EMP_DATE_INCOME',
          EMP_CARD_ID         ='$EMP_CARD_ID',
          EMP_ADDRESS1        ='$EMP_ADDRESS1',
          EMP_ADDRESS2        ='$EMP_ADDRESS2',
          EMP_ADDR_SUBDISTRICT  ='$EMP_ADDR_SUBDISTRICT',
          EMP_ADDR_DISTRICT     ='$EMP_ADDR_DISTRICT',
          EMP_ADDR_PROVINCE     ='$EMP_ADDR_PROVINCE',
          EMP_ADDR_POSTAL       ='$EMP_ADDR_POSTAL',
          EMP_EMAIL             ='$EMP_EMAIL',
          EMP_TEL               ='$EMP_TEL',
          EMP_WAGE              ='$EMP_WAGE',
          EMP_IS_TRAINER        ='$EMP_IS_TRAINER',
          EMP_IS_STAFF          ='$EMP_IS_STAFF',
          EMP_IS_SALE           ='$EMP_IS_SALE',
          EMP_IS_INSTRUCTOR     ='$EMP_IS_INSTRUCTOR'
          WHERE COMPANY_CODE = '$companycode' and EMP_CODE = '$EMP_CODE' ";
}
else if($action == "DEL"){
  $sql = "UPDATE data_mas_employee SET
  DATA_DELETE_STATUS = 'Y',
  DATA_DELETE_BY = '$user_id_update',
  DATA_DELETE_DATE = NOW()
  where EMP_CODE = '$EMP_CODE' ";

}
//echo $sql;
// --ADD EDIT USER-- //
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}
?>
