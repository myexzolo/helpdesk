<?php
session_start();
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code = $_SESSION['branchCode'];
?>

<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                    <div class="col-sm-8">
                      <input type="file" name="filepath" accept=".xlsx,.xls" required>
                    </div>
                  </div>
                  <div class="text-center col-md-12">
                    <button type="submit" class="btn btn-success btn-flat" style="width:80px;">นำเข้า</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">แสดงรายการ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="tableUpload" class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th >ลำดับ</th>
                        <th class="align-left">ชื่อไฟล์</th>
                        <th data-align="center">วันที่นำเข้า</th>
                        <th data-align="center">สถานะ</th>
                        <th data-align="center"> ทั้งหมด</th>
                        <th data-align="center">สำเร็จ</th>
                        <th data-align="center">ล้มเหลว</th>
                        <th data-align="center">เลขที่ล้มเหลว</th>
                    </tr>
                </thead>
                <tbody>
              <?php
                      $sql  = "SELECT *
                               FROM t_history_upload WHEREbranch_code = '$code' and type_file = 'user' order by history_upload_id desc";

                      $query     = DbQuery($sql,null);
                      $json       = json_decode($query, true);
                      $errorInfo  = $json['errorInfo'];
                      $dataCount  = $json['dataCount'];
                      $rows       = $json['data'];

                      for($i=0 ; $i < $dataCount ; $i++) {

                        $status_upload = $rows[$i]['status_upload'];

                        $status = "สมบูรณ์";
                        if($status_upload == "F")
                        {
                          $status = "ผิดพลาด";
                        }

                    ?>
                    <tr class="text-center">
                      <td><?=$i + 1;?></td>
                      <td><?=$rows[$i]['file_name'];?></td>
                      <td><?= DateTimeThai($rows[$i]['date_upload']);?></td>
                      <td><?= $status; ?></td>
                      <td><?=$rows[$i]['total_record'];?></td>
                      <td><?=$rows[$i]['success_record'];?></td>
                      <td><?=$rows[$i]['fail_record'];?></td>
                      <td><?=$rows[$i]['fail_list'];?></td>
                    </tr>
                    <?php } ?>
            </tbody>
            </table>
          </div>
    </div>
  </div>
</div>
<script>
$(function () {
  $('#tableUpload').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
