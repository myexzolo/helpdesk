<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
  }
</style>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>Num</th>
      <th>User Login</th>
      <th>Name</th>
      <th>LastName</th>
      <th>สาขา</th>
      <th>แผนก</th>
      <th>สถานะ</th>
      <th style="width:100px;">Re Password</th>
      <th style="width:70px;">Edit</th>
      <th style="width:70px;">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $str    = $_SESSION['member'][0]['user_id']==0?"":" and u.user_id > 0";
      //echo $str;
      $sqls   = "SELECT u.*, b.branch_name, d.department_name
                 FROM t_user u, t_branch b, t_department d
                 where u.is_active <> 'D' and u.branch_code = b.branch_code and u.department_code = d.department_id $str
                 ORDER BY u.user_id DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><?=$value['user_login']?></td>
      <td align="left"><?=$value['user_name']?></td>
      <td align="left"><?=$value['user_last'];?></td>
      <td align="left"><?=$value['branch_name'];?></td>
      <td align="left"><?=$value['department_name'];?></td>
      <td><?=$value['is_active']=='Y'?"ACTIVE":"INACTIVE";?></td>
      <td>
        <button type="button" class="btn btn-default btn-sm btn-flat" onclick="resetPass('<?=$value['user_id']?>')">Reset</button>
      </td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$value['user_id']?>')">
          <i class="fa fa-edit"></i>
        </button>
      </td>
      <td>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['user_id']?>','<?=$rows[$i]['user_login']?>')">
          <i class="fa fa-trash-o"></i>
        </button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
