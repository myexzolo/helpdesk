<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 250px; /* Set a small width */
}

.datepicker {
    padding: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 0px;
    direction: ltr;
}
th {
  text-align: center;
}
</style>
<div class="autoOverflow">
<table class="table table-bordered table-hover table-striped" id="tableDisplay" style="width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:50px">No</th>
      <th>รูป</td>
      <th>Title</th>
      <th>รายละเอียด</th>
      <th style="width:150px">วันที่เริ่มโปร</th>
      <th style="width:150px">วันที่สิ้นสุดโปร</th>
      <th style="width:90px">ราคา</th>
      <th style="width:80px">สถานะ</th>
      <th style="width:40px">Edit</th>
      <th style="width:40px">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM data_mas_package where package_status != 'D' and promotion in ('N','M','A') ORDER BY create_date DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
        $img = "../../image/promotions/mobile/".$rows[$i]['img'];
    ?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td align="center"> <img src="<?= $img;?>" class="thumbnail" onerror="this.onerror='';this.src='../../image/picture.png'"></td>
      <td align="left"><?=$rows[$i]['package_name'];?></td>
      <td align="left"><?=$rows[$i]['package_detail'];?></td>
      <td align="center"><?=DateThai($rows[$i]['start_pro_date']);?></td>
      <td align="center"><?=DateThai($rows[$i]['end_pro_date']);?></td>
      <td align="right"><?= number_format($rows[$i]['package_price'],2); ?></td>
      <td align="center"><?=$rows[$i]['package_status']=='Y'?"ACTIVE":"INACTIVE";?></td>
      <td align="center">
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['package_id']?>')">Edit</button>
        </td>
      <td align="center">
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['package_id']?>','<?=$rows[$i]['package_name']?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthMenu'  : [10,20,50,100],
     'lengthChange': true,
     'searching'   : true,
     'ordering'    : false,
     'info'        : true,
     'autoWidth'   : false
   })
  })
</script>
