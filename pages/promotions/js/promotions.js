

$(function () {
  showTable();
  $('.select2').select2();
  $('.datepicker').datepicker({
      autoclose: true,
  });
});

function genPackageCode(type,comCode)
{
  var url = "../../service/genPackageCode.php?package_type="+type+"&companycode="+comCode;
  $.get(url)
  .done(function( data ){
    $("#package_code").val( data.message );
  });
}

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}


function readURL(input,values) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
              $($.parseHTML("<img width='200'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}


function checkTypeImage(input,values){
  var file = input.files[0];
  var fileType = file["type"];
  var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  if ($.inArray(fileType, validImageTypes) < 0) {
      alert('ประเภทไฟล์ไม่ถูกต้อง');
      $('#'+values).html('');
      input.value = '';
  }
}

function showForm(value,id){
  $.post("ajax/formModule.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function del(id,name){
  $.smkConfirm({
    text:'Are You Sure Delete Promotion :'+ name +'?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',package_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#000',
            barColor: '#fff',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


$('#formAddModule').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddModule').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#000',
        barColor: '#fff',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAddModule').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
