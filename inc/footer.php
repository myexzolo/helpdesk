<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0.1
  </div>
  <strong>Copyright &copy; 2019-2020 SUTHIPAN.P<!--<a href="https://adminlte.io">Almsaeed Studio</a>.--></strong> All rights
  reserved.
</footer>

<div class="control-sidebar-bg"></div>
